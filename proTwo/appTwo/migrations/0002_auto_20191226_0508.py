# Generated by Django 2.2.5 on 2019-12-26 05:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appTwo', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='Last_name',
            new_name='last_name',
        ),
    ]
